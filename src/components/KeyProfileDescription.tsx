import React, { FunctionComponent } from "react";
import styled from "styled-components";

import { Trans } from "../i18n";

const KeyProfileDescriptionWrapper = styled.div`
  opacity: 0.55;
  white-space: nowrap;
`;

const KeyProfileDescription: FunctionComponent<{
  profilesByName: any;
  profileName: string;
  children?: React.ReactNode;
}> = ({ profilesByName, profileName, children }) => {
  const profile = profilesByName[profileName];
  if (!profile) {
    return <KeyProfileDescriptionWrapper>&nbsp;</KeyProfileDescriptionWrapper>;
  }
  return (
    <KeyProfileDescriptionWrapper>
      <Trans
        i18nKey={`index:profiles.${profileName}.description`}
        values={profile.info || {}}
      >
        {children}
      </Trans>
    </KeyProfileDescriptionWrapper>
  );
};

export default KeyProfileDescription;
